// #3-4

/*fetch('https://jsonplaceholder.typicode.com/todos')
.then((response)=>response.json())
.then((json) => console.log(json));*/

fetch('https://jsonplaceholder.typicode.com/todos') // fetch the whole collection
.then((response)=>response.json()) // it converts out json object response to js object
.then((json) => { let toDoList = json.map(function(list){
    return list.title;

})
console.log(toDoList)
});


// #5-6

fetch('https://jsonplaceholder.typicode.com/todos') // fetch the whole collection
.then((response)=>response.json()) // it converts out json object response to js object
.then((json) => console.log(`The item "${json.title}" on the list has a status of false`));

// #7

fetch('https://jsonplaceholder.typicode.com/todos/', {

	method: 'POST',

	headers: {
		'Content-type' : 'application/json'

	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
		completed: false,
		userId: 1,
		id: 201,
	})

})				//converts our json response
.then((response)=>response.json())
.then((json) => console.log(json))


// #8-9

fetch('https://jsonplaceholder.typicode.com/todos/1',
{
	method: 'PUT',
	headers: {
		'Content-type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
		description: 'To update my to do list with a different structure.',
		status: 'Pending',
		userId: 1,

		/*id: 1,
		title: 'New car',
		body: 'Just got my new red tesla',
		userId: 1,*/
	})
})
.then((response)=>response.json())
.then((json) => console.log(json));

// #10-11

fetch('https://jsonplaceholder.typicode.com/todos/1',
{
	method: 'PATCH',
	headers: {
		'Content-type' : 'application/json'
	},
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "07/09/21"
	})
})
.then((response)=>response.json())
.then((json) => console.log(json));

// #12
fetch('https://jsonplaceholder.typicode.com/todos/1',
{	method: 'DELETE',
})
.then((response)=>response.json())
.then((json) => console.log(json));